\documentclass[12pt, reqno, oneside]{amsproc}

%% Important Packages
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage[dvipsnames]{xcolor}
\usepackage[hang,flushmargin]{footmisc} % no footnote indentation
\usepackage{csquotes}
\usepackage{dsfont}
\usepackage{mathtools}
\usepackage[hidelinks]{hyperref}
\usepackage[english]{babel}
\usepackage{blindtext}
\usepackage[]{geometry}
\usepackage[nomath,lighttext, nott, nosf]{kpfonts}
\newcommand{\proofheadfont}{\itshape}


%% Useful mathematics symbols.
\newcommand{\into}{\rightarrow}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\Q}{\mathfrak{Q}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\NN}{\mathbb{N}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\FF}{\mathbb{F}}
\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\KK}{\mathbb{K}}
\newcommand{\B}{\mathcal{B}}
\renewcommand{\d}[1]{\,\mathrm{d}{#1}}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\renewcommand{\O}{\varnothing}
\newcommand{\M}{\mathds{M}_{{w}}}
\renewcommand{\epsilon}{\varepsilon}
\newcommand{\set}[1]{\left\{#1\right\}}
\renewcommand{\scshape}{\otherscshape}
\renewcommand{\textsc}{\textothersc}


%% Theorem styles
\theoremstyle{plain}
\newtheorem{thm}{Theorem}
\newtheorem{lem}[thm]{Lemma}
\newtheorem{prop}[thm]{Proposition}
\newtheorem{cor}[thm]{Corollary}

\theoremstyle{definition}
\newtheorem{def_}{Definition}

\theoremstyle{remark}
\newtheorem{remark}[thm]{Remark}
\newtheorem{ex}{Example}

\makeatletter
\let\sv@thm\@thm
\def\@thm{\let\indent\relax\sv@thm}
\makeatother

\makeatletter
\renewenvironment{proof}[1][\proofname]{\par
	\pushQED{\qed}%
	\normalfont \topsep6\p@\@plus6\p@\relax
	\trivlist
	\itemindent\z@ % original has \normalparindent
	\item[\hskip\labelsep
	\scshape
	#1\@addpunct{.}]\ignorespaces
}{%
	\popQED\endtrivlist\@endpefalse
}
\makeatother


\title[Math 475 Notes]{ { Honours Partial Differential Equations Notes}}
%    author one information
\author[E. Chernysh]{Edward Chernysh}
\thanks{Last Updated: \today.\newline
	\textsc{Math 475 Lecture Notes, McGill University.}\newline
	\textit{Instructor: Professor Rustum Choksi.}\newline
}

\begin{document}
	\maketitle
	\newpage
	\tableofcontents
	\newpage

\section*{Lecture 1}
	
	\noindent\textbf{Note:} An alternative set of notes is available on MyCourses.\footnote{The first 14 chapters of this text will be covered in this course, and not all of it.} The only requisite for the course accompanying these notes is basic real analysis, basic undergraduate ODE theory and  multivariable calculus. However, we woud like to stress an intuitive understanding of the results presented in the aforementioned courses. There are two grading schemes available and these may be found on the course page. Those taking the course should note that the midterm is non replacable.
	
	Loosely speaking, we \enquote{analyze} a real valued function $u$ defined on $\RR^n$ with $n =1,2,3$. A \textbf{partial differential equation} is an equation relating an unknown function $u$ to its partial derivatives and the independent spatial variables $$x_1,x_2,\hdots, x_n.$$ The \textbf{order} of the PDE is simply the order of the \emph{highest derivative} appearing in the equation. More often than not, the order is either one or two. 
	
	To help motivate the study of PDE we consider natural (i.e. scientific) situations in which such an equation may arise. Let $u(x_1,x_2,x_3,t)$ give the \emph{temperature} at the spatial point $(x_1,x_2,x_3)$ given the time $t$. Since heat flows from hot to cold, it is clear that there is a spatial dependence on $u$, and the rate at which this energy transfer occurs is related to the \emph{spatial gradient}: $\nabla u$. By the divergence theorem, we can obtain equations involving the Laplacian $\Delta := \nabla^2$ giving birth to the theory of harmonic functions.
	
	\begin{def_}
	Given a PDE involving a function $u$ and a domain\footnote{An open and connceted subset of $\RR^n$} $\Omega$, a particular function $u$ is a solution if it satisfies the PDE at all points in $\Omega$.
	\end{def_}

Consider $n=2$, for simplicity. A PDE may always be written in the form
$$F(x,y,u,u_x,y_y) = 0, \quad \text{in }\Omega.$$
If the reader so desires, many examples of simple PDE may be found online. In fact, there are many lists of PDE occuring in physics, chemistry and biology. Unfortunately, the PDE that occur in nature are often non-linear equations for which there is no general theory. Consider a PDE of the form
	$$
			L(u) = \text{\enquote{all terms involving only the independent variables and derivatives}}.
	$$
\begin{def_}
	Assume $L$ is as above.
	\begin{enumerate}
		\item The PDE is said to be linear if $L$ is a linear operator.
		\item The PDE is said to be nonlinear if $L$ is non-linear.
	\end{enumerate}
\end{def_}
By a linear operator, we mean that for any functions $u,v : \RR^n \into \RR$ and $c \in \RR$ there holds
	$$
		L(u+cv) = L(u) + cL(v).
	$$
\begin{description}	
		\item[General Solution] A function that \enquote{solves} the PDE but involves arbitrary functions (often due to a lack of initial information). For example, consider the equation
			$$
				u_x \equiv 0, \quad \text{in } \Omega \subseteq \RR^2
			$$
		where $u = u(x,y)$. Clearly, any solution can depend only on $y$ and thus, each solution is of the form $u(x,y) = g(y)$. Another example would be
			$$
				u_{xx} \equiv 0, \quad \text{in } \RR^2.
			$$
		If $u$ is a solution, then $u_x = f(y)$ in the whole space. Whence we find that
			$$
				u(x,y) = xf(y) + g(y), \quad \text{in } \RR^2.
			$$
		A final example:
			$$
				u_{xy} \equiv 0 ,\quad \text{in } \RR^2.
			$$
		Since $(u_x)_y \equiv 0$ we obtain $u_x \equiv f(x)$ for some function $f$. Integrating once again we obtain
			$$
				u(x,y) = F(x) + g(y), \quad \text{where } F^\prime \equiv f.\footnotemark
			$$
		\footnotetext{Of course this imposes certain regularity conditions on the solutions.}
		It is now clear that, in general, we cannot possible hope for uniqueness of solutions.\\
		
		\item[Initial Data] To solve the issue of uniqueness we need to impose initial conditions on the PDE. This often includes information regarding the behaviour of solutions on the boundary of a domain.
\end{description}

\subsection*{First Order Equations \& the Method of Characteristics}
We begin this part with an example of this method.

\begin{ex}
	$au_x + bu_y = 0$ in $\RR^2$ with $a,b \in \RR$. Equivalently,	
		$$
			(a,b) \cdot \nabla u \equiv 0 , \quad \text{in } \RR^2.
		$$
	This is essentially saying \enquote{the directional derivative of $u$ in the direction of the vector $(a,b)$ is zero}. Geometrically, this means that any solution is constant along lines with direction $(a,b)$. Certainly, fix a line $\ell$ parallel to the vector $(a,b)$ in $\RR^2$. Any such line is of the form
		$$
			bx-ay = c
		$$
	where $c \in \RR$ determines the line uniquely. The claim is that a $C^1(\RR^2)$ solution $u$ is constant along any such line. To see that this is so, note that we may parametrize $u$ solely in terms of $x$ along $\ell$ since $y = \frac{b}{a}x - \frac{c}{a}$. Hence, we may write for some $C^1$ function $f$:
		$$
			u(x,y) = f(x) \quad \text{on } \ell.
		$$
	It then suffices to show that $f^\prime$ vanishing entirely on this line. To see this differentiate with respect to $x$ to obtain by the chain rule
	\begin{align*}
		f^\prime(x) = \frac{\d{}}{\d{x}}\,u\left(x,  \frac{bx}{a} - \frac{c}{a} \right)
			= u_x(x,y) + \frac{b}{a} u_y(x,y)
			= (a,b) \cdot \nabla u(x,y) = 0
	\end{align*}
yielding the desired result. Now, it follows from this that $u$ depends only on the line parallel to $(a,b)$ given. Hence, any solution is of the form
	$$
		u(x,y) = g(bx-ay)=g(c).
	$$
\end{ex}

\begin{ex}
	We apply this to the problem
		$$
			\begin{cases}
				4u_x - 3u_y \equiv 0, & \text{in } \RR^n,\\
				u \equiv y^3, & \text{on } x = 0.
			\end{cases}
		$$
By the previous example, we know any classical\footnote{Given a PDE let $\alpha$ denote the order of the highest derivative. A solution $u$ is called classical if it belongs to $C^\alpha(\RR^n)$.} solution takes the form $u(x,y) = f(-3x-4y)$ for some function $f$. Now, on the $y$-axis we have that
	$$
		u(x,y) = f(-3x-4y) = f(-4y) = y^3.
	$$
Taking $\xi := -4y$ we find that $f(\xi) = \frac{-\xi^3}{64}$ which implies
	$$
		u(x,y) = f(-3x-4y) = \frac{(3x+4y)^3}{64}.
	$$
\end{ex}


\section*{Lecture 2}

Before we develop in further detail the method of characteristics we wish to consider another \enquote{computational} example. The more examples we give, the more intuition we will have regarding these characteristics.

\begin{ex}[Time Dependent Transport Equation]
	By way of an example consider the problem
		$$
			\begin{cases}
				u_t + cu_x\equiv 0, & \text{for } (x,t) \in \RR \times [0,\infty),\\
				u(x,0) = g(x).
			\end{cases}
		$$
	By now we know that we should rewrite this as $(c,1) \cdot \nabla u(x,t) \equiv 0$ and that the general solution will be of the form $u(x,t) = f(x-ct)$ for some function $f$. Taking $t=0$ we find that
		$$
			u(x,0) = f(x) = g(x)
		$$
	it follows that $f(x) = g(x)$ and hence that $u(x,t) = g(x-ct)$.
\end{ex}

\begin{ex}
	Consider the equation $u_x + y u_y \equiv 0$. This is an equation of the form
		$$
			(1,y) \cdot \nabla u(x,y) = 0
		$$
	at all points. Thus, given a point $(x,y)$ the solutions are constant along any line in the direction of $(1,y)$. The idea is to now \enquote{solve} for such curves. This is merely an ODE problem:
		$$
			\frac{\d{y}}{\d{x}} = y
		$$
	which has the solution $y(x) = Ce^x$ for a given constant $C$. Given a point $(x,y)$ in space this value of $C$ is determined (uniquely) by
		$$	
			y(x)e^{-x}
		$$
	and so, along these lines, we have $u(x,y) = f(C) = f(ye^{-x})$.
\end{ex}

\begin{ex}
	Assume $u$ solves $au_x +bu_y + u \equiv 0$ in space. This is an equation of the form $(a,b) \cdot \nabla u + u \equiv 0$. But now the solution is not necessarily constant on a characteristic! The idea is to make the change of variables $x^\prime = ax+by$ and $y^\prime = bx-ay$.
\end{ex}

\subsection*{The Method of Characteristics}

There are many places online where one can find a \enquote{recipe} for the method of characteristics. We believe that the best approach to learning this method is to see it in practice, with plenty of detail. We shall introduce the notation
	$$
		\mathbb{H} := \left\{ (x,y) \in \RR^2  : x> 0 \right\}
	$$
and write $\Gamma$ to denote the $x$-axis. We interested in smooth solutions to the problem
	$$
		\begin{cases}
			-yu_x + xu_y \equiv u, & \text{in } \mathbb{H},\\
			u(x,0) = g(x),
		\end{cases}
	$$
where $g$ is given. We seek a parametrization of \enquote{characteristics} that are yet to be determined. Loosely speaking, we look for curves that have the rate $(-y, x)$ at a given point $(x,y)$. Why? This is important because then we can use the information given by the PDE in
	$$
		(-y,x) \cdot \nabla u(x,y) = u(x,y).
	$$
We seek to determine a parametrized curve $(x(t), y(t))$ such that
	$$
		x^\prime(s) = -y(s), \quad  y^\prime(s) = x(s).
	$$
There is no harm in putting $z(s) = u(x(s), y(s))$ (the reason for this will become clear in a moment). This system grants us the relation
	$$
		x^{\prime\prime}(s) + x(s) = 0.
	$$
The characteristic equation for this is $m^2 +1 = 0$ which has solutions $\pm i$. Hence, a general solution appears in the form
	$$
		x(s) = c_1 \cos{x} + c_2\sin{s}.
	$$
Now, using that $y^\prime(s) = x(s) = c_1 \cos{x} + c_2\sin{s}$ we obtain 
	$$
		y(s) = c_1\sin{s} - c_2\cos{s}.
	$$
It is straightforward to calculate
	\begin{align*}
	z^\prime(s) = u_x x^\prime(s) + u_y y^\prime(s) = -yu_x + xu_u = u(x,y) = z(s)
	\end{align*}
whence we obtain $z(s) = c_3 e^s = u(x,y)$ at all points. Now we need to use the initial data. We need to choose our constants so that at $s=0$ we lie at a unique point on $\Gamma$. Fix $x_0 \in \Gamma$ and observe that the choice of constants
	$$
		x^\prime(s) = x_0\cos{s}, \quad y^\prime(s) = x_0\sin{s}
	$$
is sufficient. At $s=0$ we must have $u(x,y) = g(x_0)$ showing that we must take $c_3 = g(x_0)$. So,
	$$
		u(x,y) = g(x_0)e^s.
	$$
Clearly, we take\footnote{We opt for the arctangent to rid ourselves of the $x_0$.}
	$$
		s= \arctan\left( \frac{y}{x}\right)
	$$
and $x_0 = \sqrt{x^2 + y^2}$ and the rest is a matter of substitution.\\

Now, with the aim of developing a general method we consider an equation in two independent variables of the form
	$$
		a(x,y)u_x + b(x,y)u_y = c_1(x,y)u + c_2(x,y), \quad \text{in a domain } \Omega \subset \RR^2.
	$$
where we are given initial data on some curve $\Gamma \subset \overline{\Omega}$. We will attempt to describe the characteristics with parameters in the form
	$$
		(x(s), y(s)), \quad s \in \RR.
	$$
The previous examples clearly say that we should solve the equations:
	$$
		\frac{\d{x}}{\d{s}} = a(x(s), y(s)), \quad
		\frac{\d{y}}{\d{s}} = b(x(s), y(s)).
	$$
Again, we shall put $z(s) := u(x(s), y(s))$ to describe the solution along these characteristic curves. Then, differentiation with respect to our variable $s$ gives
	\begin{align*}
		z^\prime(s) = u_x x^\prime(x) + u_y y^\prime(s)
			&= a(x,y)u_x + b(x,y) u_y \\
			&= c_1(x(s), y(s))u(x(s), y(s)) + c_2(x(s), y(s)).
	\end{align*}
Alternatively, we may use the notation $z^\prime(s) = \dot{z}(s)$.

We now return to previously solved equations from this new and clearly outlined perspective.

\begin{ex}
	Consider the equation
		$$
			\begin{cases}
				au_x + bu_y \equiv 0, & \text{in } \RR^2,\\
				u(x,0)  = x^3
			\end{cases}
		$$
	for $a,b \in \RR$. We now define
		$$
			\dot{x}(s) = a, \quad \dot{y} = b, \quad \dot{z} = 0.
		$$
	These have solutions of the form $x(s) = as + c_1$, $y(s) = bs + c_2$ and $z(s) = c_3$. So that we lie on the so called \enquote{data axis} it is wise to take $c_2 = 0$ and label
$c_1$ as $x_0$. Hence, we are free to choose
	$$
		x(s) = as + x_0, \quad y(s) = bs, \quad \text{and } z(s) = u(x(s),y(s)) = x_0^3.
	$$
Now, given a point $(x,y)$ we must determine which characteristic it lies on using our equations above. We have that $s = \frac{y}{b}$ and $x_0 = x - \frac{ay}{b}$. Thus, the solution is
	$$
		u(x,y) = \left( x - \frac{ay}{b}\right)^3.
	$$
\end{ex}

\section*{Lecture 3}
 
 Consider, once again, the transport equation
 	$$
 		u_t + cu_x = 0, \quad \text{with } u(x,0) - g(x)
 	$$
 and $c > 0$. Note that we already have a general solution for this PDE.  Indeed, we know that $u(x,t) = g(cx - t)$. Ask yourself, what will the stationary solutions look like at a time $t$? What happens to these curves as $t$ increases? (Translated to the right).
 
\subsection*{Method of Characteristics for Quasilinear Equations}

These are equations of the form
	$$
		a(x,y,u) u_x + b(x,y,u)u_y = c(x,y,u).
	$$
Following the method of characteristics we should take
	$$
		\dot{x}(s) = a(x(s), y(s), z(s) ),\quad
		\dot{y}(s) = b(x(s), y(s), z(s)),
	$$
and $z(s) = u(x(s), y(s))$. Now,
	$$
		\dot{z}(s) = u_x \dot{x}(s) + u_y\dot{y}(s)  = c(x(s), y(s), z(s)).
	$$
In certain cases, we can explicitly solve this system of ODEs which lands us in the \enquote{simple cases} we saw previously.

\begin{ex}
	Consider again $u_t + u_x = 0$ with $u(x,0) = g(x)$. We have the \enquote{characteristic system}	
		$$
			\dot{t}(s) = 1 \implies t = s.
		$$
	Hence, we can write our work in terms of $t$. Now, $\dot{x}(s) = z(s)$ and $\dot{z}(s) = 0$ so that $z(s) = c$. Given our initial data we should take 
		$$
			z(0) = u(x(0), t) = u(x(0), s) = u(x, 0) = g(x_0)
		$$
	where $x_0 = x(0)$. Thus, $z(t) = g(x_0)$. Carrying on in this way we obtain
		$$
			\frac{x-x_0}{t} = g(x_0)
		$$
	since $z$ is constant along these characteristics. 
\end{ex}

\section*{Lecture 4}

We consider the one dimensional wave equation:
	$$
		u_{tt} = c^2 u_{xx}
	$$
where $c^2 = T/\rho$; $T$ and $\rho$ are the tension and density of a rope. We need to specify initial displacement:
	$$
		u(x,0) = \phi(x)
	$$
and  $u_t (x,0) = \psi(x)$. This equation can be written as
	$$
		(\partial_t +c\partial_x)(\partial_t - c\partial_x)u = 0.
	$$
\begin{prop}
	The general solution to the $1\mathrm{D}$ wave equation is	
		$$
			u(x,t) = f(x+ct) + g(x-ct).
		$$
\end{prop}	
\begin{proof}
	Put $\zeta = x+ct$ and $\eta = x-ct$. In these new coordinates,	
		$$
			u(x,t) =u(\zeta,\eta) 
		$$
	as well as
		$$
			u_x = u_\zeta \cdot \frac{\partial \zeta}{\partial x} + u_\eta \cdot \frac{\partial \eta}{\partial x} = u_\zeta + u_\eta
		$$
	and
		$$
			u_t = u_\zeta c - u_\eta c.
			$$
	In our notation, $\partial_x = \partial_\zeta + \partial_\eta$ and $\partial_t = c\partial_\zeta - c\partial_\eta$. hence,
		$$
			\partial_t - c\partial_x = -2c\partial_\eta u, \quad	
			\partial_t + c\partial_x = 2x\partial_\zeta u.
		$$
	and thus $u_{n\zeta}= 0$. This concludes the proof.
\end{proof}
	
\end{document}
