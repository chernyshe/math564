# Advanced Real Analysis 1 Lecture Notes

This repository contains the lecture-notes source (in LaTeX) for Advanced Real Analysis 1 (Math 564) given in Fall 2017 at McGill University. You may find here the source and PDF files. Alternatively, you may download the PDF [here](http://cs.mcgill.ca/~echern2/repo/).
